//
//  Bundle.swift
//  Exchange
//
//  Created by Andy Lee on 8/7/18.
//  Copyright © 2018 Atom International. All rights reserved.
//

import Foundation

extension Bundle {
    var version: String? {
        return Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String
    }
    
    var build: String? {
        return Bundle.main.object(forInfoDictionaryKey: kCFBundleVersionKey as String) as? String
    }
}
