//
//  UserInfoAPI.swift
//  Exchange
//
//  Created by Andy Lee on 8/7/18.
//  Copyright © 2018 Atom International. All rights reserved.
//

import Foundation
import Moya
import Alamofire

// MARK: - Provider setup

private func JSONResponseDataFormatter(_ data: Data) -> Data {
    do {
        let dataAsJSON = try JSONSerialization.jsonObject(with: data)
        let prettyData =  try JSONSerialization.data(withJSONObject: dataAsJSON, options: .prettyPrinted)
        return prettyData
    } catch {
        return data // fallback to original data if it can't be serialized.
    }
}

let gitHubProvider = MoyaProvider<UserInfo>(plugins: [NetworkLoggerPlugin(verbose: true, responseDataFormatter: JSONResponseDataFormatter)])

// MARK: - Provider support

private extension String {
    var urlEscaped: String {
        return self.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }
}

// MARL: - Define API

public enum UserInfo {
    case user
    case info(String)
}

extension UserInfo: TargetType {
    public var baseURL: URL { return URL(string: "https://api.aixapi.com")! }
    public var path: String {
        switch self {
        case .user:
            return "/user"
        case .info(let name):
            return "/info/\(name.urlEscaped)"
        }
    }
    public var method: Moya.Method {
        return .get
    }
    public var task: Task {
        switch self {
        default:
            return .requestPlain
        }
    }

    public var sampleData: Data {
        switch self {
        case .user:
            return "Half measures are as bad as nothing at all.".data(using: String.Encoding.utf8)!
        case .info(let name):
            return "{\"login\": \"\(name)\", \"id\": 100}".data(using: String.Encoding.utf8)!
        }
    }
    public var headers: [String: String]? {
        return nil
    }
}

public func url(_ route: TargetType) -> String {
    return route.baseURL.appendingPathComponent(route.path).absoluteString
}

// MARK: - Response Handlers

extension Moya.Response {
    func mapNSArray() throws -> NSArray {
        let any = try self.mapJSON()
        guard let array = any as? NSArray else {
            throw MoyaError.jsonMapping(self)
        }
        return array
    }
}
