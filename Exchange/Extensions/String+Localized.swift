//
//  String+Localized.swift
//  Exchange
//
//  Created by Andy Lee on 8/7/18.
//  Copyright © 2018 Atom International. All rights reserved.
//

import Foundation

extension String {
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
}
